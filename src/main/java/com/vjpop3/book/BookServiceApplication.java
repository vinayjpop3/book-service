package com.vjpop3.book;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(
        //https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(
                title = "Book API",
                version = "0.0",
                description = "Book service application API",
                contact = @Contact(url = "http://book-service.com", name = "Vinay Lakkam", email = "vinay_lakkam@epam.com")
        )
)
public class BookServiceApplication {
  public static void main(String[] args) {
    SpringApplication.run(BookServiceApplication.class, args);
  }

  /*@Bean
  public OpenAPI customOpenAPI(@Value("${springdoc.version}") String appVersion) {
    return new OpenAPI()
            .info(new Info()
                    .title("Book API2")
                    .version(appVersion)
                    .description("Book service application"));
  }*/
}
