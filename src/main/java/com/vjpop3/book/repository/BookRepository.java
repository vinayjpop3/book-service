package com.vjpop3.book.repository;

import com.vjpop3.book.domain.Book;

import org.springframework.data.jpa.repository.JpaRepository;

//@RepositoryRestResource
public interface BookRepository extends JpaRepository<Book, Long> {

}
