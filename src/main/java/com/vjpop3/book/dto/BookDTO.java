package com.vjpop3.book.dto;


import com.vjpop3.book.domain.Book;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Size;

@Schema(name="Book")
public class BookDTO {

  Long id;

  @Size(min = 2)
  String title;

  String genre;

  public BookDTO() {
  }

  public BookDTO(Book book) {
    this.id = book.getId();
    this.title = book.getTitle();
    this.genre = book.getGenre();
  }

  public BookDTO(Long id, String title, String genre) {
    this.id = id;
    this.title = title;
    this.genre = genre;
  }

  public BookDTO(@Size(min = 2) String title, String genre) {
    this.title = title;
    this.genre = genre;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getGenre() {
    return genre;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

  @Override
  public String toString() {
    return "Book{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", genre='" + genre + '\'' +
            '}';
  }
}
