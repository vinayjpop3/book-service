package com.vjpop3.book.rest;

import com.vjpop3.book.dto.BookDTO;
import com.vjpop3.book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/v2/books")
public class BookResourceV2 {

  @Autowired
  BookService bookService;

  /*@GetMapping
  ResponseEntity<CollectionModel<EntityModel<BookDTO>>> getAll() {
    final List<EntityModel<BookDTO>> books = bookService.findAll().stream().
            map(book -> {
              final Link selfLink = linkTo(methodOn(BookResourceV2.class).get(book.getId())).withSelfRel();
              return new EntityModel<Book>(book, selfLink);
            })
            .collect(toList());

    final CollectionModel<EntityModel<Book>> booksModel = new CollectionModel<>(books,
            linkTo(methodOn(BookResourceV2.class).getAll()).withSelfRel());

    return ResponseEntity.ok(booksModel);
  }

  @GetMapping("{bookId}")
  ResponseEntity get(@PathVariable Long bookId) {
    final Book book = bookService.findById(bookId);
    Link selfLink = linkTo(methodOn(BookResourceV2.class).get(bookId)).withSelfRel();
    Link allBooksLink = linkTo(methodOn(BookResourceV2.class).getAll()).withRel("all-books");

    EntityModel bookModel = new EntityModel<Book>(book, selfLink, allBooksLink);
    return ResponseEntity.ok(bookModel);
  }*/

  //TODO: POST, PUT, DELETE
}
