package com.vjpop3.book.rest;

import com.vjpop3.book.dto.BookDTO;
import com.vjpop3.book.error.ExceptionResponse;
import com.vjpop3.book.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookResource {

  private final Logger log = LoggerFactory.getLogger(BookResource.class);

  @Autowired
  BookService bookService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Get all books", description = "Get list of books")
  @ApiResponse(responseCode = "200", description = "Operation successful")
  ResponseEntity<List<BookDTO>> findAll() {
    return ResponseEntity
            .ok(bookService.findAll());
  }

  @GetMapping(value = "{bookId}", produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Get a book by id", description = "Returns a book by id")
  @ApiResponse(responseCode = "200", description = "Operation successful")
  @ApiResponse(
          responseCode = "404", description = "Book not found",
          content = @Content(schema = @Schema(implementation = ExceptionResponse.class), mediaType = "application/json")
  )
  ResponseEntity<BookDTO> get(@Parameter(description = "Book Id")
                              @PathVariable Long bookId) {
    log.debug("REST request to get Book : {}", bookId);

    return ResponseEntity.ok(bookService.findById(bookId));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Create a book", description = "Creates a new book")
  @ApiResponse(responseCode = "201", description = "Operation successful",
          headers = {@Header(name = "Location", description = "Location of the create book resource")}
  )
  @ApiResponse(
          responseCode = "400", description = "Invalid book data",
          content = @Content(schema = @Schema(implementation = ExceptionResponse.class))
  )
  ResponseEntity<BookDTO> create(@RequestBody @Valid BookDTO bookDTO) {
    log.debug("REST request to save Book : {}", bookDTO);

    BookDTO savedBook = bookService.create(bookDTO);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(savedBook.getId())
            .toUri();

    return ResponseEntity
            .created(location)
            .body(savedBook);
  }

  @PutMapping(value = "{bookId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Update a book", description = "Updates an existing book by id")
  @ApiResponse( responseCode = "201", description = "Operation successful")
  @ApiResponse(
          responseCode = "404", description = "Book not found",
          content = @Content(schema = @Schema(implementation = ExceptionResponse.class))
  )
  @ApiResponse(
          responseCode = "400", description = "Invalid book data",
          content = @Content(schema = @Schema(implementation = ExceptionResponse.class), mediaType = "application/json")
  )
  ResponseEntity<BookDTO> update(@Parameter(description = "Book Id") @PathVariable Long bookId,
                                 @RequestBody BookDTO bookDTO) {
    log.debug("REST request to update Product : {}", bookDTO);

    final URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentRequest().toUriString());
    return ResponseEntity.created(uri).body(bookService.update(bookId, bookDTO));
  }

  @DeleteMapping(value = "{bookId}")
  @Operation(summary = "Delete a book", description = "Updates an existing book by id")
  @ApiResponse(responseCode = "204", description = "Operation successful")
  @ApiResponse(
          responseCode = "404", description = "Book not found",
          content = @Content(schema = @Schema(implementation = ExceptionResponse.class), mediaType = "application/json")
  )
  ResponseEntity<?> delete(@Parameter(description = "Book Id") @PathVariable Long bookId) {
    bookService.delete(bookId);
    return ResponseEntity.noContent().build();
  }
}