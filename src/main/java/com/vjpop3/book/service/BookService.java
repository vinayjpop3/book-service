package com.vjpop3.book.service;

import com.vjpop3.book.domain.Book;
import com.vjpop3.book.dto.BookDTO;
import com.vjpop3.book.error.BookNotFoundException;
import com.vjpop3.book.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

@Service
public class BookService {

  BookRepository bookRepository;

  @Autowired
  public BookService(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  public List<BookDTO> findAll() {
    return bookRepository.findAll().stream()
            .map(BookDTO::new)
            .collect(toList());
  }

  public BookDTO create(BookDTO bookDTO) {
    Book book = new Book(bookDTO.getTitle(), bookDTO.getGenre());
    return new BookDTO(bookRepository.save(book));
  }

  public BookDTO update(Long bookId, BookDTO bookDTO) {
    Book book = new Book(bookId, bookDTO.getTitle(), bookDTO.getGenre());
    return bookRepository
            .findById(bookId)
            .map(existingBook -> bookRepository.save(book))
            .map(BookDTO::new)
            .orElseThrow(bookNotFound(bookId));
  }

  public BookDTO findById(Long bookId) {
    return bookRepository
            .findById(bookId)
            .map(BookDTO::new)
            .orElseThrow(bookNotFound(bookId));
  }

  public void delete(Long bookId) {
    final Runnable bookNotFoundException = () -> {
      throw new BookNotFoundException("Book not found: " + bookId);
    };

    bookRepository.findById(bookId)
            .ifPresentOrElse(bookRepository::delete,
                    bookNotFoundException); // complex? :)
  }

  private Supplier<BookNotFoundException> bookNotFound(Long bookId) {
    return () -> new BookNotFoundException("Book not found: " + bookId);
  }
}
