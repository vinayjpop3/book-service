package com.vjpop3.book.repository;

import com.vjpop3.book.domain.Book;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

@DataJpaTest
class BookRepositoryTest {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  TestEntityManager entityManager;

  @Test
  void testFindAll() {
    this.entityManager.persist(new Book("test1", "genre1"));
    this.entityManager.persist(new Book("test2", "genre2"));

    final List<Book> books = this.bookRepository.findAll();
    Assertions.assertThat(books.size()).isEqualTo(4); // 2 records exists already
  }

  @Test
  void testSave() {
    Book book = bookRepository.save(new Book("test1", "genre1"));
    Assertions.assertThat(bookRepository.findById(book.getId()).isPresent());
  }
  // Other methods doesn't need tests
}