package com.vjpop3.book.service;

import com.vjpop3.book.domain.Book;
import com.vjpop3.book.dto.BookDTO;
import com.vjpop3.book.error.BookNotFoundException;
import com.vjpop3.book.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;

/**
 * Unit tests for {@link BookService}
 */
@ExtendWith(SpringExtension.class)
class BookServiceTest {

  @MockBean
  private BookRepository bookRepository;

  BookService bookService;

  @BeforeEach
  public void before() {
    bookService = new BookService(bookRepository);
  }

  @Test
  void givenBooks_whenFindAll_thenReturnBooks() {
    final Book bookInDB = new Book(1L, "Title1", "Genre1");
    given(this.bookRepository.findAll()).willReturn(Collections.singletonList(bookInDB));

    List<BookDTO> books = bookService.findAll();

    assertThat(books.size()).isEqualTo(1);
    assertThat(books.get(0).getId()).isEqualTo(1L);
    assertThat(books.get(0).getTitle()).isEqualTo("Title1");
    assertThat(books.get(0).getGenre()).isEqualTo("Genre1");
  }

  @Test
  void givenBook_whenFindById_thenReturnBook() {
    final Book bookInDB = new Book(1L, "Title1", "Genre1");
    given(this.bookRepository.findById(1L)).willReturn(Optional.of(bookInDB));

    BookDTO book = bookService.findById(1L);

    assertThat(book.getId()).isEqualTo(1L);
    assertThat(book.getTitle()).isEqualTo("Title1");
    assertThat(book.getGenre()).isEqualTo("Genre1");
  }

  @Test
  void givenNoBook_whenFindById_thenReturnError() {
    given(this.bookRepository.findById(1L)).willReturn(Optional.empty());

    assertThatCode(() -> bookService.findById(1L))
            .isInstanceOf(BookNotFoundException.class)
            .hasMessageContaining("Book not found: 1");
  }

  @Test
  void whenCreateBook_thenReturnBook() {
    given(this.bookRepository.save(any(Book.class)))
            .willReturn(new Book(1L, "Title1", "Genre1"));

    final BookDTO book = new BookDTO("Title1", "Genre1");
    BookDTO createdBook = bookService.create(book);

    assertThat(createdBook.getId()).isEqualTo(1L);
    assertThat(createdBook.getTitle()).isEqualTo("Title1");
    assertThat(createdBook.getGenre()).isEqualTo("Genre1");
  }

  @Test
  void givenBook_whenUpdate_thenReturnBook() {
    final Book bookInDB = new Book(1L, "Title1", "Genre1");
    given(this.bookRepository.findById(1L)).willReturn(Optional.of(bookInDB));
    final Book updatedBookInDB = new Book(1L, "Updated Title1", "Updated Genre1");
    given(this.bookRepository.save(any(Book.class))).willReturn(updatedBookInDB);

    final BookDTO bookToUpdate = new BookDTO(1L, "Updated Title1", "Updated Genre1");
    final BookDTO updatedBook = bookService.update(1L, bookToUpdate);

    assertThat(updatedBook.getId()).isEqualTo(1L);
    assertThat(updatedBook.getTitle()).isEqualTo("Updated Title1");
    assertThat(updatedBook.getGenre()).isEqualTo("Updated Genre1");
  }

  @Test
  void givenNoBook_whenUpdate_thenReturnError() {
    given(this.bookRepository.findById(1L)).willReturn(Optional.empty());

    assertThatCode(() -> bookService.update(1L, new BookDTO()))
            .isInstanceOf(BookNotFoundException.class)
            .hasMessageContaining("Book not found: 1");
  }

  @Test
  void givenBook_whenDelete_thenReturnVoid() {
    final Book bookInDB = new Book(1L, "Title1", "Genre1");
    given(this.bookRepository.findById(1L)).willReturn(Optional.of(bookInDB));
    willDoNothing().given(this.bookRepository).delete(any(Book.class));

    bookService.delete(1L);
  }


  @Test
  void givenNoBook_whenDelete_thenReturnError() {
    given(this.bookRepository.findById(1L)).willReturn(Optional.empty());

    assertThatCode(() -> bookService.delete(1L))
            .isInstanceOf(BookNotFoundException.class)
            .hasMessageContaining("Book not found: 1");
  }
}