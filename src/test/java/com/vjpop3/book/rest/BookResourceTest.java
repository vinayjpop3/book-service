package com.vjpop3.book.rest;

import com.vjpop3.book.dto.BookDTO;
import com.vjpop3.book.error.BookNotFoundException;
import com.vjpop3.book.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Integration test for {@link BookResource} using test slice
 * <p>
 * To test whether Spring MVC controllers are working as expected, use the @WebMvcTest annotation.
 *
 * @WebMvcTest auto-configures the Spring MVC infrastructure and limits scanned beans to @Controller, @ControllerAdvice, @JsonComponent, Converter, GenericConverter, Filter, HandlerInterceptor, WebMvcConfigurer, and HandlerMethodArgumentResolver.
 * Regular @Component beans are not scanned when using this annotation.
 */
@WebMvcTest(BookResource.class)
class BookResourceTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private BookService bookService;

  @Test
  void givenBooks_whenFindAllBooks_thenStatus200() throws Exception {
    given(this.bookService.findAll()).willReturn(Collections.emptyList());
    this.mvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
  }

  @Test
  void givenBook_whenGetBook_thenStatus200() throws Exception {
    BookDTO book = new BookDTO(1L, "Test", "Genre");

    given(this.bookService.findById(1L)).willReturn(book);

    this.mvc.perform(get("/books/1").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title", is("Test")));
  }

  @Test
  void givenBookNotExists_whenGetBook_thenStatus404() throws Exception {
    given(this.bookService.findById(1L)).willThrow(new BookNotFoundException("Book not found"));

    this.mvc.perform(get("/books/1"))
            .andExpect(status().isNotFound());
  }

  @Test
  void whenCreateBook_thenStatus201() throws Exception {
    BookDTO book = new BookDTO(1L, "Test", "Genre");
    given(this.bookService.create(any(BookDTO.class))).willReturn(book);

    String bookJson = "{\"title\": \"Test\", \"genre\": \"genre\"}";
    final MockHttpServletRequestBuilder request = post("/books")
            .content(bookJson)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON);

    this.mvc.perform(request)
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(header().exists("Location"))
            //.andExpect(header().string("Location", "http://localhost/books/1")) //TODO: Works; but not a good idea
            .andExpect(jsonPath("$.title", is("Test")));
  }

  @Test
  void whenCreateBookWithBadData_thenStatus400() throws Exception {
    BookDTO book = new BookDTO(1L, "Test", "Genre");
    given(this.bookService.create(any(BookDTO.class))).willReturn(book);

    String bookJson = "{\"title\": \"T\", \"genre\": \"genre\"}"; // invalid title
    final MockHttpServletRequestBuilder request = post("/books")
            .content(bookJson)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON);

    this.mvc.perform(request)
            .andExpect(status().isBadRequest());
  }

  @Test
  void givenBook_whenUpdateBook_thenStatus201() throws Exception {
    BookDTO book = new BookDTO(1L, "Updated Test", "Updated Genre");
    given(this.bookService.update(anyLong(), any(BookDTO.class))).willReturn(book);

    String bookJson = "{\"id\":1, \"title\": \"Updated Test\", \"genre\": \"Updated genre\"}";
    final MockHttpServletRequestBuilder request = put("/books/1")
            .content(bookJson)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON);

    this.mvc.perform(request)
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.title", is("Updated Test")));
  }

  @Test
  void givenBook_whenDeleteBook_thenStatus204() throws Exception {
    willDoNothing().given(this.bookService).delete(1L);
    this.mvc.perform(delete("/books/1"))
            .andExpect(status().isNoContent());
  }
}